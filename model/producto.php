<?php
//clase pojo, una clase pojo es una clase plana que incluye variables, get and set.

require_once 'bd.php';

Class Producto{

    private $id;
    private $nombre;

    private $pdo;

    public function __CONSTRUCT(){
        $this->pdo = BD::Conectar();
    }

    public function getId() :?string{
        return $this->id;
    }

    public function setId(string $id){
        $this->id = $id;
    }

    public function getNombre() :?string{
        return $this->nombre;
    }

    public function setNombre(string $nombre){
        $this->nombre = $nombre;
    }

    /* El siguiente paso es crear los metodos del mantenedor de la tabla trabajada */ 

    public function Insertar(producto $producto){
        try {
            $consulta = "insert into producto(id, nombre) values (?,?)";
            $this->pdo->prepare($consulta)->execute(array($producto->getId(), $producto->getNombre()));
        } catch (Exception $e) {
           die($e->getMessage());
        }


    }

}

